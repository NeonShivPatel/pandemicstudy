#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 18 10:07:18 2021

@author: Shiv Patel
@author: Edris Zoghlami
"""
from file_io import FileIO
import mysql.connector

class archiver:
    
    #When the archiver object is created we will connect to a mysql DB, and have
    # the db and the cursor as field to be used
    def __init__(self):
        self.mydb = mysql.connector.connect(
            host="localhost",
            user="shiv",
            password="dawson123",
            database="testDB",
            auth_plugin='mysql_native_password'
            )  
        self.mycursor = self.mydb.cursor(buffered = True)

    #dropTable method will drop the table
    def dropTable(self):
        try:
            self.mycursor.execute("DROP TABLE countries")
        except:
            print("Table doesn't exist yet")
            
    #makeTable method will create the table and insert data into the table
    def makeTable(self):
        self.dropTable()
        self.mycursor.execute("""CREATE TABLE countries (id INT AUTO_INCREMENT PRIMARY KEY, 
        Country VARCHAR(40), TotalCases INT, NewCases INT, TotalDeaths INT, NewDeaths INT, 
        TotalRecovered INT, NewRecovered INT, ActiveCases INT, Serious INT, TotCasesPer1M DECIMAL(10,1), 
        DeathsPer1M DECIMAL(10,1), TotalTests INT, TestsPer1M DECIMAL(10,1), Population INT, Continent VARCHAR(20), Day VARCHAR(20))""")
        jsonFile = FileIO("countries.json")
        data = jsonFile.getJSON()
        self.addData(data, "today_country")
        self.addData(data, "yesterday_country")
        self.addData(data, "yesterday2_country")
        print("Table Made")

    #addData method is used to insert data into the table
    def addData(self, data, key):
        sql = """INSERT INTO countries (Country, TotalCases, NewCases, TotalDeaths, NewDeaths, 
        TotalRecovered, NewRecovered, ActiveCases, Serious, TotCasesPer1M, DeathsPer1M, 
        TotalTests, TestsPer1M, Population, Continent, Day) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"""
        val = []
        for country in data[key]:
            # print("The New Case is : ", country["NewCases"], " , of type: ", type(country["NewCases"]), ", on the day: ", country["Day"])
            tup = (country["Country"],country["TotalCases"],country["NewCases"],country["TotalDeaths"],
               country["NewDeaths"],country["TotalRecovered"],country["NewRecovered"],
               country["ActiveCases"],country["Serious"],country["Tot Cases/1M pop"],country["Deaths/1M pop"],
               country["TotalTests"],country["Tests/1M pop"],country["Population"],country["Continent"],country["Day"])
            val.append(tup)
        self.mycursor.executemany(sql, val)