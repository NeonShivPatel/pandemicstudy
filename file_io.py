#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 23 18:06:19 2021

@author: Shiv Patel
@author: Edris Zoghlami
"""
import json

class FileIO:
    
    #When a FileIO object is made, a filename for the file will be saved as a field
    def __init__(self, filename):
        self.filename=filename
    
    #getLines method will return all the data inside a file
    def getLines(self):
        with open("/Users/mukeshpatel/Documents/pandemicstudy/" + self.filename,"r", encoding='ISO-8859-1') as file:
                fileLines = file.readlines()
        return fileLines
    
    #makeFile method will create a file containing the data inside a tbody variable
    def makeFile(self, tbody):
        txt_file = open('/Users/mukeshpatel/Documents/pandemicstudy/' + self.filename, 'w', encoding= 'ISO-8859-1')
        i = 0
        j = 0
        for row in tbody.find_all('tr'):
            if "total_row_world" in str(row):
                #print(row)
                i = i+1
            else:
                txt_file.write(str(row.text))
                j = j+ 1
        # print("My Lines: " + str(j))
        # print("Others: " + str(i))
        txt_file.close()

    #getJSON is a method that will get the data of a json file
    def getJSON(self):
        with open("/Users/mukeshpatel/Documents/pandemicstudy/" + self.filename) as json_file:
            data = json.load(json_file)
            print("Json Data Given")
            return data
        