#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 18 10:08:40 2021

@author: Shiv Patel
@author: Edris Zoghlami
"""
from scraping import scraper
from archiving import archiver

#Scrapping the data from the corona website and making a json file
scraper2 = scraper("https://www.worldometers.info/coronavirus")
scraper2.createJSON()

#This will store the scrapped data inside a table in the mysql database
archiver2 = archiver()
archiver2.makeTable()

#What's left to do is create the objects to plot and do data science
#We can also add methods to delete the txt files if needed
#And we need to have better naming sense, and delete the print()'s

#This is to test out what went inside the database is actually good
sql = "Select DeathsPer1M FROM countries"
archiver2.mycursor.execute(sql)
myresult = archiver2.mycursor.fetchall() 
for x in myresult: 
  print(x[0])
print("DONEZO")

archiver2.mydb.commit() 
archiver2.mycursor.close()
archiver2.mydb.close()
