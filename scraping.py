#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 18 10:04:49 2021

@author: Shiv Patel
@author: Edris Zoghlami
"""
from file_io import FileIO
from bs4 import BeautifulSoup
import requests
import json

class scraper:
    
    #When we make the scrapper object, we will be adding the url of the site we are scrapping
    # Into a special field
    def __init__(self, url):
        self.url = url
    
    #makeSoup method will use the BeautifulSoup package to get the html content of the url page
    def makeSoup(self):
        req = requests.get(self.url)
        self.soup = BeautifulSoup(req.content, "html.parser")
       
    #createFile method will use a given filename and tableID to create a txt file, 
    # made out of the data of a specific table
    def createFile(self, filename, tableID):
        table = self.soup.find("table", id=tableID)
        tbody = table.find('tbody',attrs={'class': None})
        control = FileIO(filename)
        control.makeFile(tbody)
    
    #createJSON is a method that will call upon various other methods to 
    # create a single JSON file containing data on all countries for the 3 days requested
    def createJSON(self):
        self.makeSoup()
        self.createFile("today.txt", "main_table_countries_today")
        self.createFile("yesterday.txt", "main_table_countries_yesterday")
        self.createFile("yesterday2.txt", "main_table_countries_yesterday2")
        maindict = self.makeDict("today.txt", "yesterday.txt", "yesterday2.txt")
        with open("/Users/mukeshpatel/Documents/pandemicstudy/countries.json","w") as write_file:
            json.dump(maindict,write_file)
        print("JSON Created")
    
    #cleanData method will go into a file, and replace empty lines with a "None" string
    def cleanData(self, filename):
        control = FileIO(filename)
        tableLines = control.getLines()
        y = 0
        for x in tableLines:
            if (tableLines[y].replace("\n","") == "" or tableLines[y].replace("\n","") == " "):
                tableLines[y]= "None"
                y = y+1
        return tableLines
        print("Data Cleaned")
    
    #changeData acts similarly to cleanData, however the key difference is that we are now transforming
    # the data into the correct format for the mysql database
    def changeData(self, x):
        if (x == "None" or x== "N/A\n" or x == " " or x == "" or x == "N/A"):
            return None
        else:
            try:
                if (int(float(x)) == float(x)):
                     return int(x)
                else:
                     # print("Ok so this is supposed to be a float: ", type(x))
                     return float(x)
            except:
                return x
    
    #makeSubDict is a method that will go through the data of a txt file 
    # and make a dictionnary for every country, and then return an arr containing 
    #  those dictionnaries
    def makeSubDict(self, tableLines, day):
        arr = []
        i=0
        while(i<len(tableLines)):
            subdict ={
                "Day": day,
                "#": self.changeData(tableLines[i+1]),
                "Country" : self.changeData(tableLines[i+2].replace("\n","").replace("\n","")),
                "TotalCases": self.changeData(tableLines[i+3].replace(",","").replace("\n","")),
                "NewCases" : self.changeData(tableLines[i+4].replace(",","").replace("\n","")),
                "TotalDeaths" : self.changeData(tableLines[i+5].replace(",","").replace("\n","")),
                "NewDeaths" : self.changeData(tableLines[i+6].replace(",","").replace("\n","")),
                "TotalRecovered" : self.changeData(tableLines[i+7].replace(",","").replace("\n","")),
                "NewRecovered" : self.changeData(tableLines[i+8].replace(",","").replace("\n","")),
                "ActiveCases" : self.changeData(tableLines[i+9].replace(",","").replace("\n","")),
                "Serious" : self.changeData(tableLines[i+10].replace(",","").replace("\n","")),
                "Tot Cases/1M pop" : self.changeData(tableLines[i+11].replace(",","").replace("\n","")),
                "Deaths/1M pop" : self.changeData(tableLines[i+12].replace(",","").replace("\n","")),
                "TotalTests" : self.changeData(tableLines[i+13].replace(",","").replace("\n","")),
                "Tests/1M pop" : self.changeData(tableLines[i+14].replace(",","").replace("\n","")),
                "Population" : self.changeData(tableLines[i+15].replace(",","").replace("\n","")),
                "Continent" : self.changeData(tableLines[i+16].replace("\n",""))
            }
            arr.append(subdict)
            # print(subdict)
            i=i+18
        return arr
    
    #makeDict method will make and return the main dictionnary that will 
    # contain data from all 3 different days
    def makeDict(self, filename1, filename2, filename3):
        todayLines = self.cleanData(filename1)
        yesterdayLines = self.cleanData(filename2)
        yesterday2Lines = self.cleanData(filename3)
        listtoday= self.makeSubDict(todayLines, "Today")
        listyesterday= self.makeSubDict(yesterdayLines, "Yesterday")
        listyesterday2= self.makeSubDict(yesterday2Lines, "Yesterday2")
        maindict = {
            "today_country" : listtoday,
            "yesterday_country" : listyesterday,
            "yesterday2_country" : listyesterday2
            }
        return maindict
        print("Dictionnary Made")